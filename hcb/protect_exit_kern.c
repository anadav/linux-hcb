#include <linux/version.h>
#include <linux/ring_buffer_types.h>
#include <uapi/linux/bpf.h>
/* TODO: try to fix these includes to be more robust */
#include "../kernel/trace/trace.h"
#include "../arch/x86/include/asm/kvm_host.h"
#include "../arch/x86/include/asm/pvclock.h"
#include "../arch/x86/include/asm/pvclock-abi.h"
#include "../arch/x86/include/uapi/asm/vmx.h"
/* TODO: find which file to really incldue */
#include "../include/linux/smpboot.h"
#include "../include/linux/smp.h"
#include "../include/linux/kvm_host.h"
#include "../kernel/smpboot.h"

#include "vm_bpf_helpers.h"
#include "bpf_vm.h"

#define PROG(F) SEC("usercb/"__stringify(F)) int bpf_func_##F

static __always_inline void* variable_memset(char *dst, char c, size_t size)
{
	bpf_memset(host_ptr(dst), size, c);

	return dst;
}
#define memset(d, c, s)		variable_memset(d, c, s)

static __always_inline void vmcall(struct hcb_exit __host *h)
{
	struct __hcb_area __host *a = &h->area;
	u64 nr, start, len, end, aligned_start, aligned_end;
	unsigned long *bitmap = (unsigned long*)a->vm_base;

	end = start + len;
	nr = bpf_get_vcpu_register(h, VCPU_REGS_RAX);

	if (nr != 9999)
		return;

	start = bpf_get_vcpu_register(h, HCB_REG_RBX);
	len = bpf_get_vcpu_register(h, HCB_REG_RCX);

	aligned_start = round_up(start, BITS_PER_BYTE);
	aligned_end = round_down(end, BITS_PER_BYTE);

	/* XXX: Should be atomic_or */
	bitmap[start / BITS_PER_LONG] |= ((u64)(-1)) << (aligned_start - start);
	bitmap[end / BITS_PER_LONG] |= ((u64)(-1)) >> (end - aligned_start);

	/* TODO: fix because we miss the first and last bytes */
	memset((char*)&bitmap[aligned_start / BITS_PER_LONG], 0xff,
	       (aligned_end - aligned_start) / BITS_PER_BYTE);
}

static __always_inline void ept_violation(struct hcb_exit __host *h)
{
	struct __hcb_area __host *a = &h->area;
	unsigned long *bitmap = (unsigned long*)a->vm_base;
	u64 qualification;
	gpa_t gpa;
	gfn_t gfn;

	gpa = bpf_get_exit_info(h, HCB_EXIT_INFO_GUEST_PHYSICAL_ADDRESS);
	gfn = gpa_to_gfn(gpa);

	if (!test_bit(gfn, bitmap))
		return;

	qualification = bpf_get_exit_info(h,
				   HCB_EXIT_INFO_GUEST_EXIT_QUALIFICATION);

	if (!(qualification & PFERR_WRITE_MASK))
		return;

	bpf_set_vcpu_register(h, HCB_REG_IDTR, 0, 0);
	bpf_send_vcpu_self_ipi(get_ctx(), 2);
}

SEC("usercb/vm_protect_exit")
int bpf_prog1(struct hcb_exit __host *h)
{
	volatile u64 ign = cache_ctx((void*)(u64)h);

	switch (h->exit_reason) {
	case EXIT_REASON_VMCALL:
		vmcall(h);
		break;
	case EXIT_REASON_EPT_VIOLATION:
		ept_violation(h);
		break;
	}

	return 0;
}



char _license[] SEC("license") = "GPL";
u32 _version SEC("version") = LINUX_VERSION_CODE;
