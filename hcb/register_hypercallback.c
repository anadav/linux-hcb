#define _GNU_SOURCE         
#include <unistd.h>
#include <sys/syscall.h> 

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <getopt.h>
#include <errno.h>


static void usage_error(char **argv)
{
	fprintf(stderr, "usage: %s -f [filename] -c [hcb_num]\n", argv[0]);
	exit(-1);
}

int main(int argc, char **argv)
{
	const char *filename = NULL;
	char c;
	FILE *file;
	char *prog;
	size_t sz, read_sz;
	long err = -1;
	int hcb_num = -1;

	while ((c = getopt (argc, argv, "hf:c:")) != -1) {
		switch (c) {
		case 'f':
			filename = optarg;
			break;
		case '?':
		case 'h':
			usage_error(argv);
			break;
		case 'c':
			hcb_num = atoi(optarg);
			break;
		}
	}

	if (filename == NULL)
		usage_error(argv);

	if (hcb_num < 0)
		usage_error(argv);

	file = fopen(filename, "rb");
	if (file == NULL) {
		perror("fopen");
		goto out;
	}

	/* find the size */
	fseek(file, 0l, SEEK_END);
	sz = ftell(file);
	fseek(file, 0l, SEEK_SET);

	prog = malloc(sz);
	if (prog == NULL) {
		perror("malloc");
		goto out;
	}

	read_sz = fread(prog, 1, sz, file);
	if (read_sz != sz) {
		perror("fread");
		goto out;
	}

	err = syscall(/*__NR_register_hypercallback*/329, hcb_num, prog, sz);
	if (err) {
		fprintf(stderr, "hypercallback registration failed: %s\n", strerror(errno));
	}

	err = 0;
out:
	fclose(file);	

	return err;
}
