#include <linux/version.h>
#include <linux/pagemap.h>
#include <uapi/linux/bpf.h>
#include <uapi/linux/seccomp.h>
#include <uapi/linux/kvm_para.h>
#include <linux/mm_types.h>

#include "../mm/internal.h"

#include "vm_bpf_helpers.h"
#include "bpf_vm.h"

static __always_inline unsigned long page_struct_pfn(unsigned long pfn)
{
	struct hcb_vmemmap *v = (struct hcb_vmemmap*)read_ctx(env[HCB_ENV_BASE]);

	unsigned long page_struct_offset = pfn * sizeof(struct page);

	return v->page_struct_pfn[page_struct_offset >> HPAGE_PMD_SHIFT] |
		((page_struct_offset & ~HPAGE_PMD_MASK & PAGE_MASK) >>
		 PAGE_SHIFT);
}

static __always_inline
struct page *bpf_pfn_to_page(struct hcb_vmemmap *v, unsigned long pfn)
{
	struct page *page_start = (struct page*)(v->page_struct);

	return page_start + pfn;
}

static __always_inline struct page *page_to_alt_mapping(struct hcb_vmemmap *v,
							struct page *page)
{
	unsigned long pfn = page_to_pfn(page);

	return bpf_pfn_to_page(v, pfn);
}

static __always_inline int bpf_trylock_page(struct hcb_vmemmap *v,
					    struct page *page, unsigned long pfn)
{
	struct page *head = compound_head(page);

	if (head != page) {
		/*
		 * since we work with altenrative mappings we need to convert
		 * back to our points
		 */
		page = page_to_alt_mapping(v, head);
	}

	if (bpf_exclusive_access(get_ctx(), page_struct_pfn(pfn)))
		return false;

	if (test_bit(PG_locked, &page->flags))
		return false;
	return true;
}

static __always_inline void bpf_unlock_page(struct page *page)
{
	/* XXX: nothing to do, since the lock was never really taken */
}
#define unlock_page(p)			bpf_unlock_page(p)

#ifdef CONFIG_SPARSEMEM_VMEMMAP

#undef __page_to_pfn
#undef page_to_pfn
#undef __pfn_to_page
#undef pfn_to_page

#if 0
#undef vmemmap
#define vmemmap			((struct page*)vm_sym_ptr(_etext))
#endif

#undef pfn_to_page
#undef vmemmap

#define page_to_pfn(page)	(unsigned long)((page) - bpf_vmemmap)
#define pfn_to_page(pfn)	(bpf_vmemmap + (pfn))

#else
static_assert(1, "only sparsmem_vmemmap supported");
#endif

/*
 * We have few changes from the original version: we use spin_trylock(), we
 * unroll the loop, and we provide the pfn to avoid the relatively expensive
 * computation that the compiler would not optimize for us.
 */
static __always_inline bool bpf_is_free_buddy_page(struct page *page,
						   unsigned long pfn)
{
	struct zone *zone = page_zone(page);
	unsigned long flags;
	unsigned int order = 0;

#pragma unroll
	for (order = 0; order < MAX_ORDER; order++) {
		struct page *page_head = page - (pfn & ((1 << order) - 1));

		if (PageBuddy(page_head) && page_order(page_head) >= order)
			break;
	}

	return order < MAX_ORDER;
}

SEC("usercb/vm_freemem")
int bpf_prog1(struct hcb_page __host *hdp)
{
	volatile u64 ign = cache_ctx((void*)(u64)hdp);
	unsigned long gfn = hdp->gpa >> PAGE_SHIFT;
	int r;
	struct hcb_vmemmap *v = (struct hcb_vmemmap*)hdp->area.env[HCB_ENV_BASE];
	struct page *page = bpf_pfn_to_page(v, gfn);

	hdp->area.err_line  = (u32)gfn;
	r = bpf_is_free_buddy_page(page, gfn);
	return r;
}

char _license[] SEC("license") = "GPL";
u32 _version SEC("version") = LINUX_VERSION_CODE;

