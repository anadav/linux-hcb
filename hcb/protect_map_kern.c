#include <linux/version.h>
#include <linux/ring_buffer_types.h>
#include <uapi/linux/bpf.h>
/* TODO: try to fix these includes to be more robust */
#include "../kernel/trace/trace.h"
#include "../arch/x86/include/asm/kvm_host.h"
#include "../arch/x86/include/asm/pvclock.h"
#include "../arch/x86/include/asm/pvclock-abi.h"
/* TODO: find which file to really incldue */
#include "../include/linux/smpboot.h"
#include "../include/linux/smp.h"
#include "../kernel/smpboot.h"
#include "../arch/x86/kvm/mmu.h"

#include "vm_bpf_helpers.h"
#include "bpf_vm.h"

#define PROG(F) SEC("usercb/"__stringify(F)) int bpf_func_##F

#define MAX_BIT_RANGE	(BITS_PER_LONG * 8)
#define ACC_WRITE_MASK   PT_WRITABLE_MASK

static __always_inline
bool test_bit_range(unsigned long *bitmap, u64 start, u64 len)
{
	int i, j;
	u64 *p;
 	u64 end = start + len;
	u64 start_i, end_i, mask, r = 0;
 
 	if (len > MAX_BIT_RANGE)
 		return false;	// cannot test
 
	mask = ((u64)-1) << (start % BITS_PER_LONG);
	r |= bitmap[start / BITS_PER_LONG] & mask;

	start_i = DIV_ROUND_UP(start, BITS_PER_LONG);
	end_i = end / BITS_PER_LONG;

	i = start_i;
#pragma unroll
	for (j = 0; j < MAX_BIT_RANGE / BITS_PER_LONG; j++) {
		if (i >= end)
			break;
		r |= bitmap[i++];
	}
 
	mask = ((u64)-1) >> (BITS_PER_LONG - (end % BITS_PER_LONG));
	r |= bitmap[end_i] & mask;
 
	return !!r;
}

SEC("usercb/vm_protect_map")
int bpf_prog1(struct hcb_map_range __host *h)
{
	volatile u64 ign = cache_ctx((void*)(u64)h);
	struct __hcb_area __host *a = &h->area;
	unsigned long *bitmap = (unsigned long*)a->vm_base;
 
	if (test_bit_range(bitmap, h->gfn, h->count))
		return ACC_WRITE_MASK;
 
	return 0;
}
 
char _license[] SEC("license") = "GPL";
u32 _version SEC("version") = LINUX_VERSION_CODE;
