#include <linux/version.h>
#include <linux/ring_buffer_types.h>
#include <uapi/linux/bpf.h>
/* TODO: try to fix these includes to be more robust */
#include "../kernel/trace/trace.h"
#include "../arch/x86/include/asm/kvm_host.h"
#include "../arch/x86/include/asm/pvclock.h"
#include "../arch/x86/include/asm/pvclock-abi.h"
/* TODO: find which file to really incldue */
#include "../arch/x86/include/asm/hardirq.h"
#include "../arch/x86/include/asm/tlbflush.h"
#include "../include/linux/smpboot.h"
#include "../include/linux/smp.h"
#include "../kernel/smpboot.h"

#include "vm_bpf_helpers.h"
#include "bpf_vm.h"

#define PROG(F) SEC("usercb/"__stringify(F)) int bpf_func_##F

/*
 * local operations are non-preemptible so we can don't care how the compiler
 * treats them
 */

extern void flush_tlb_func(void *info);

/* no tracing */
#define trace_tlb_flush_rcuidle(a, b)

static __always_inline void bpf_leave_mm(int cpu)
{
	struct tlb_state *cur_tlbstate = this_cpu_ptr(KSYM(cpu_tlbstate));
	struct mm_struct *active_mm = cur_tlbstate->active_mm;

//	if (cur_tlbstate->state == TLBSTATE_OK)
//		BUG();
	if (cpumask_test_cpu(cpu, mm_cpumask(active_mm))) {
		cpumask_clear_cpu(cpu, mm_cpumask(active_mm));
		//load_cr3(KSYM(swapper_pg_dir));
		load_cr3(*(KSYM(init_level4_pgt)));	/* XXX: cleanup */
		/*
		 * This gets called in the idle path where RCU
		 * functions differently.  Tracing normally
		 * uses RCU, so we have to call the tracepoint
		 * specially here.
		 */
		//trace_tlb_flush_rcuidle(TLB_FLUSH_ON_TASK_SWITCH, TLB_FLUSH_ALL);
	}
}

#define leave_mm(cpu)			bpf_leave_mm(cpu)

static __always_inline void bpf_local_flush_tlb()
{
	bpf_flush_tlb_vcpu(get_ctx());
}

static __always_inline void bpf__flush_tlb_single(unsigned long addr)
{
	/*
	 * We should have a separate helper for PTE-specific invalidations.
	 * But anyhow, any reasonable implementation would just flush the
	 * entire TLB, so for now, there is a single helper.
	 */
	bpf_flush_tlb_vcpu(get_ctx());
}

#undef local_flush_tlb
#define local_flush_tlb()		bpf_local_flush_tlb()


static __always_inline void bpf_flush_tlb_func(void *info)
{
	struct flush_tlb_info *f = info;
	struct tlb_state *cur_tlbstate = this_cpu_ptr(KSYM(cpu_tlbstate));

	// inc_irq_stat(irq_tlb_count);

	if (f->flush_mm && f->flush_mm != cur_tlbstate->active_mm)
		return;

	//count_vm_tlb_event(NR_TLB_REMOTE_FLUSH_RECEIVED);
	if (cur_tlbstate->state == TLBSTATE_OK) {
		/*
		 * In order not to modify the code we could have "mimic"
		 * individual PTE invalidations, but it really makes no sense.
		 */
		if (f->flush_end == TLB_FLUSH_ALL) {
			local_flush_tlb();
			/* TODO: how to trace? */
	//		trace_tlb_flush(TLB_REMOTE_SHOOTDOWN, TLB_FLUSH_ALL);
		}
	} else
		leave_mm(smp_processor_id());
}

#undef raw_local_save_flags
#define raw_local_save_flags(flags)						\
	do {									\
		flags = bpf_get_vcpu_register(get_ctx(), HCB_REG_RFLAGS);	\
	} while (0)

static __always_inline void csd_unlock(struct call_single_data *csd)
{
	//WARN_ON(!(csd->flags & CSD_FLAG_LOCK));

	/*
	 * ensure we're all done before releasing data:
	 */
	smp_store_release(&csd->flags, 0);
}

static __always_inline bool handle_ipi_to_sleeping_core(int vector)
{
	struct call_single_data *csd;
	struct llist_head *head;
	struct llist_node *entry, *old_entry, *next;

	if (vector != CALL_FUNCTION_VECTOR &&
	    vector != CALL_FUNCTION_SINGLE_VECTOR)
		return false;

	/*
	 * Nobody is supposed to remove stuff from our list. So we are going to
	 * read the first entry, and see that we can handle it. If we can, then
	 * we will remove it from the list and handle it.
	 */
	head = this_cpu_ptr(KSYM(call_single_queue));

	/* open-coded ... */
	entry = smp_load_acquire(&head->first);

	/* If for some reason there is  no work to do, stop */
	if (entry == NULL) {
		WARN();
		return false;
	}

	next = READ_ONCE(entry->next);

	/* More than a single unit of work and we cannot loop... */
	if (next) {
		WARN();
		return false;
	}

	csd = llist_entry(entry, typeof(*csd), llist);

	/* Check we can do it */
	if ((ulong)csd->func != (ulong)(PKSYM(flush_tlb_func))) {
		WARN();
		return false;
	}

	/* Ok, we can handle it; try to take the first piece of work */
	old_entry = entry;

	entry = cmpxchg(&head->first, old_entry, next);

	/* something else got enqueued? if true, we cannot handle */
	if (entry != old_entry)
		return false;

	/*
	 * We are going to handle a single IPI; if there are more, we will
	 * just say they are unhandlable
	 */
	if (1 || (ulong)csd->func == (ulong)(PKSYM(flush_tlb_func))) {
		/*
		 * due to the lack of function pointers, we will
		 * just assume now that TLB shootdown is sync.
		 */
		if (irqs_disabled())
			local_flush_tlb();
		else
			bpf_flush_tlb_func(csd->info);
		csd_unlock(csd);
	}
	return true;
}

SEC("usercb/vm_ipi")
int bpf_prog1(struct __hcb_ipi __host *ht)
{
	volatile u64 ign = cache_ctx((void*)(u64)ht);
	struct __hcb_area __host *a = &ht->area;
	//unsigned flags;
	//int vector = 0;

	/* TODO: get the vector from the area */
#if 0

	flags = bpf_get_vcpu_rflags(get_ctx());
	if (irqs_disabled_flags(flags))
		return 0;
#endif
	return handle_ipi_to_sleeping_core(ht->vector);
}



char _license[] SEC("license") = "GPL";
u32 _version SEC("version") = LINUX_VERSION_CODE;
