#ifndef __VM_BPF_HELPERS_H
#define __VM_BPF_HELPERS_H

#define SEC(NAME) __attribute__((section(NAME), used))

#define __host		__attribute__((address_space(1)))

/*
 * We are limited by BPF convention in which the size follows the pointer.
 * Currently it is not used, but hopefully we would be able to move to a newer
 * version of the verifier which does.
 */

static unsigned long long (*bpf_send_vcpu_self_ipi)(void __host *ctx,
						    int vector) =
	(void *) BPF_FUNC_send_vcpu_self_ipi;
static unsigned long long (*bpf_get_vcpu_register)(void __host *ctx, int reg) =
	(void *) BPF_FUNC_get_vcpu_register;
//static unsigned long long (*bpf_get_vcpu_msr)(void __host *ctx, int msr) =
//	(void *) BPF_FUNC_get_vcpu_msr;
//static unsigned long long (*bpf_get_vcpu_rflags)(void __host *ctx) =
//	(void *) BPF_FUNC_get_vcpu_rflags;
static unsigned long long (*bpf_memcpy)(void __host *dst,
					size_t len,
					const void __host *src,
					size_t len2) =
	(void *) BPF_FUNC_memcpy;
static unsigned long long (*bpf_memset)(void __host *dst, size_t len,
					char c) =
	(void *) BPF_FUNC_memset;
static unsigned long long (*bpf_cmpxchg)(void __host *ptr, int size,
		unsigned long o, unsigned long n) =
	(void *) BPF_FUNC_cmpxchg;
static unsigned long long (*bpf_rdtsc)(void __host *ctx) =
	(void *) BPF_FUNC_rdtsc;
static unsigned long long (*bpf_set_vcpu_register)(void __host *ctx, int cr,
		unsigned long val, unsigned long val_ex) =
	(void *) BPF_FUNC_set_vcpu_register;
static unsigned long long (*bpf_flush_tlb_vcpu)(void __host *ctx) =
	(void *) BPF_FUNC_flush_vcpu_tlb;
static unsigned long long (*bpf_clear_bit)(unsigned int bit,
					   unsigned char __host *ptr) =
	(void *) BPF_FUNC_clear_bit;
static unsigned long long (*bpf_exclusive_access)(void __host *ctx,
		unsigned long long gfn) =
	(void *) BPF_FUNC_exclusive_access;
static unsigned long long (*bpf_get_exit_info)(void __host *ctx, int field) =
	(void *) BPF_FUNC_get_exit_info;
#endif
