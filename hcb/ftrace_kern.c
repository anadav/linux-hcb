#include <linux/version.h>
#include <linux/ring_buffer_types.h>
#include <uapi/linux/bpf.h>
/* TODO: try to fix these includes to be more robust */
#include "../kernel/trace/trace.h"
#include "../arch/x86/include/asm/kvm_host.h"
#include "../arch/x86/include/asm/pvclock.h"
#include "../arch/x86/include/asm/pvclock-abi.h"

#include "vm_bpf_helpers.h"
#include "bpf_vm.h"


static __always_inline struct task_struct *bpf_get_current(void)
{
	return this_cpu_read(KSYM(current_task));
}

/* XXX: find a way to do a compiler barrier; most likely it will be an overkill */
#undef barrier
#define barrier()	do {} while (0)

#undef virt_rmb
#define virt_rmb() barrier()

/*
 * local operations are non-preemptible so we can don't care how the compiler
 * treats them.
 */

static __always_inline int test_time_stamp(u64 delta)
{
	if (delta & TS_DELTA_TEST)
		return 1;
	return 0;
}

static __always_inline
void rb_start_commit(struct ring_buffer_per_cpu *cpu_buffer)
{
	local_inc(&cpu_buffer->committing);
	local_inc(&cpu_buffer->commits);
}

static __always_inline unsigned rb_calculate_event_length(unsigned length)
{
	struct ring_buffer_event event; /* Used only for sizeof array */

	/* zero length can cause confusions */
	if (!length)
		length++;

	if (length > RB_MAX_SMALL_DATA || RB_FORCE_8BYTE_ALIGNMENT)
		length += sizeof(event.array[0]);

	length += RB_EVNT_HDR_SIZE;
	length = ALIGN(length, RB_ARCH_ALIGNMENT);

	/*
	 * In case the time delta is larger than the 27 bits for it
	 * in the header, we need to add a timestamp. If another
	 * event comes in when trying to discard this one to increase
	 * the length, then the timestamp will be added in the allocated
	 * space of this event. If length is bigger than the size needed
	 * for the TIME_EXTEND, then padding has to be used. The events
	 * length must be either RB_LEN_TIME_EXTEND, or greater than or equal
	 * to RB_LEN_TIME_EXTEND + 8, as 8 is the minimum size for padding.
	 * As length is a multiple of 4, we only need to worry if it
	 * is 12 (RB_LEN_TIME_EXTEND + 4).
	 */
	if (length == RB_LEN_TIME_EXTEND + RB_ALIGNMENT)
		length += RB_ALIGNMENT;

	return length;
}

extern u8 pvclock_valid_flags;
extern atomic64_t pvclock_last_value;

static __always_inline
u64 bpf_pvclock_scale_delta(u64 delta, u32 mul_frac, int shift)
{
	u64 product;

	if (shift < 0)
		delta >>= -shift;
	else
		delta <<= shift;

	product = ((u64)mul_frac * (u32)delta) >> 32;
	product += ((u64)mul_frac * (u32)(delta >> 32));

	return product;
}

#define rdtsc_ordered()			bpf_rdtsc(get_ctx())
#define pvclock_scale_delta(a, b, c)	bpf_pvclock_scale_delta(a, b, c)

/*
 * BPF for some reason does not like packed structures and creates awful code
 * for them. Use unpacked structures and make sure they are consistent with the
 * packed ones.
 */
struct unpacked_pvclock_vcpu_time_info {
	u32   version;
	u32   pad0;
	u64   tsc_timestamp;
	u64   system_time;
	u32   tsc_to_system_mul;
	s8    tsc_shift;
	u8    flags;
	u8    pad[2];
};

BUILD_BUG_ON(sizeof(struct pvclock_vcpu_time_info) !=
	     sizeof(struct unpacked_pvclock_vcpu_time_info));

#define pvclock_vcpu_time_info		unpacked_pvclock_vcpu_time_info

static __always_inline
unsigned bpf_pvclock_read_begin(const struct pvclock_vcpu_time_info *src)
{
	unsigned version = src->version & ~1;
	/* Make sure that the version is read before the data. */
	virt_rmb();
	return version;
}

static __always_inline
bool bpf_pvclock_read_retry(const struct pvclock_vcpu_time_info *src,
			    unsigned version)
{
	/* Make sure that the version is re-read after the data. */
	virt_rmb();
	return unlikely(version != src->version);
}

static __always_inline cycle_t bpf_rdtsc_ordered(void)
{
	return bpf_rdtsc(get_ctx());
}

#define rdstc_ordered()		bpf_rdtsc_ordered()

static __always_inline
cycle_t bpf__pvclock_read_cycles(const struct pvclock_vcpu_time_info *src)
{
	u64 delta = rdtsc_ordered() - src->tsc_timestamp;
	cycle_t offset = pvclock_scale_delta(delta, src->tsc_to_system_mul,
					     src->tsc_shift);
	return src->system_time + offset;
}

#define __pvclock_read_cycles(src)	bpf__pvclock_read_cycles(src)

static __always_inline
cycle_t bpf_pvclock_clocksource_read(struct pvclock_vcpu_time_info *src)
{
	unsigned version;
	cycle_t ret;
	u64 last;
	u8 flags;

	version = bpf_pvclock_read_begin(src);
	ret = __pvclock_read_cycles(src);
	flags = src->flags;

	BPF_BUG_ON(bpf_pvclock_read_retry(src, version));

	if (unlikely((flags & PVCLOCK_GUEST_STOPPED) != 0)) {
		src->flags &= ~PVCLOCK_GUEST_STOPPED;
		//pvclock_touch_watchdogs();
		BPF_BUG_ON(1);
	}

	if ((*vm_sym_ptr(pvclock_valid_flags) & PVCLOCK_TSC_STABLE_BIT) &&
		(flags & PVCLOCK_TSC_STABLE_BIT))
		return ret;
	/*
	 * Assumption here is that last_value, a global accumulator, always goes
	 * forward. If we are less than that, we should not be much smaller.
	 * We assume there is an error marging we're inside, and then the correction
	 * does not sacrifice accuracy.
	 *
	 * For reads: global may have changed between test and return,
	 * but this means someone else updated poked the clock at a later time.
	 * We just need to make sure we are not seeing a backwards event.
	 *
	 * For updates: last_value = ret is not enough, since two vcpus could be
	 * updating at the same time, and one of them could be slightly behind,
	 * making the assumption that last_value always go forward fail to hold.
	 */
	last = atomic64_read(vm_sym_ptr(pvclock_last_value));

	if (ret < last)
		return last;
	last = atomic64_cmpxchg(vm_sym_ptr(pvclock_last_value), last, ret);

	/* We cannot loop */
	BPF_BUG_ON(unlikely(last != ret));
	return ret;
}

#define pvclock_clocksource_read(s)	bpf_pvclock_clocksource_read(s)

extern struct pvclock_vsyscall_time_info *hv_clock;

static __always_inline cycle_t kvm_clock_read(void)
{
	struct pvclock_vcpu_time_info *src;
	cycle_t ret;
	int cpu;

	preempt_disable_notrace();
	cpu = smp_processor_id();
	/* Due to the unpacked hack, we need to cast to the "unpacked" type */
	src = (struct pvclock_vcpu_time_info*)&((vm_sym_ptr(hv_clock))[cpu]->pvti);
	ret = pvclock_clocksource_read(src);
	preempt_enable_notrace();
	return ret;
}

static __always_inline cycle_t kvm_clock_get_cycles(struct clocksource *cs)
{
	return kvm_clock_read();
}

static __always_inline u64 bpf_trace_clock_local(void)
{
	u64 clock;

	/* XXX: We do not have virtual functions, so we only use KVM clock */
	clock = kvm_clock_read();

	return clock;
}

#define DEBUG_SHIFT 0

static __always_inline u64 rb_time_stamp(struct ring_buffer *buffer)
{
	/* shift to debug/test normalization and TIME_EXTENTS */

	/*
	 * XXX: in the original rb_time_stamp, a function pointer is used, which
	 * we do not support (we could have done a switch). In the meanwhile get
	 * the trace clock from the VM.
	 */
	return bpf_trace_clock_local() << DEBUG_SHIFT;
}

static __always_inline struct list_head *bpf_rb_list_head(struct list_head *list)
{
	unsigned long val = (unsigned long)list;

	return (struct list_head *)(val & ~RB_FLAG_MASK);
}
#define rb_list_head(l)			bpf_rb_list_head(l)

static __always_inline int
rb_is_head_page(struct ring_buffer_per_cpu *cpu_buffer,
		struct buffer_page *page, struct list_head *list)
{
	unsigned long val;

	val = (unsigned long)list->next;

	if ((val & ~RB_FLAG_MASK) != (unsigned long)&page->list)
		return RB_PAGE_MOVED;

	return val & RB_FLAG_MASK;
}

static __always_inline
bool rb_is_reader_page(struct buffer_page *page)
{
	struct list_head *list = page->list.prev;

	return rb_list_head(list->next) != &page->list;
}

static __always_inline unsigned long rb_page_entries(struct buffer_page *bpage)
{
	return local_read(&bpage->entries) & RB_WRITE_MASK;
}

static __always_inline
int rb_head_page_set(struct ring_buffer_per_cpu *cpu_buffer,
		     struct buffer_page *head, struct buffer_page *prev,
		     int old_flag, int new_flag)
{
	struct list_head *list;
	unsigned long val = (unsigned long)&head->list;
	unsigned long ret;

	list = &prev->list;

	val &= ~RB_FLAG_MASK;

	ret = cmpxchg((unsigned long *)&list->next,
		      val | old_flag, val | new_flag);

	/* check if the reader took the page */
	if ((ret & ~RB_FLAG_MASK) != val)
		return RB_PAGE_MOVED;

	return ret & RB_FLAG_MASK;
}

static __always_inline
int rb_head_page_set_update(struct ring_buffer_per_cpu *cpu_buffer,
			    struct buffer_page *head, struct buffer_page *prev,
			    int old_flag)
{
	return rb_head_page_set(cpu_buffer, head, prev,
				old_flag, RB_PAGE_UPDATE);
}

static __always_inline
int rb_head_page_set_head(struct ring_buffer_per_cpu *cpu_buffer,
				 struct buffer_page *head,
				 struct buffer_page *prev,
				 int old_flag)
{
	return rb_head_page_set(cpu_buffer, head, prev,
				old_flag, RB_PAGE_HEAD);
}

static __always_inline
int rb_head_page_set_normal(struct ring_buffer_per_cpu *cpu_buffer,
				   struct buffer_page *head,
				   struct buffer_page *prev,
				   int old_flag)
{
	return rb_head_page_set(cpu_buffer, head, prev,
				old_flag, RB_PAGE_NORMAL);
}

/*
 * For some reason the verifier does not like when we try to change *bpage so
 * instead return the result
 */
static __always_inline void rb_inc_page(struct ring_buffer_per_cpu *cpu_buffer,
			       struct buffer_page **bpage)
{
	struct list_head *p = rb_list_head((*bpage)->list.next);

	*bpage = list_entry(p, struct buffer_page, list);
}

/*
 * Changes:
 * 1. We should never have NORMAL and we never retry anyhow
 * 2. Try to optimize the checks to avoid the switch.
 */
static __always_inline int
bpf_rb_handle_head_page(struct ring_buffer_per_cpu *cpu_buffer,
			struct buffer_page *tail_page,
			struct buffer_page *next_page)
{
	struct buffer_page *new_head;
	int entries;
	int type;
	int ret;

	entries = rb_page_entries(next_page);

	/*
	 * The hard part is here. We need to move the head
	 * forward, and protect against both readers on
	 * other CPUs and writers coming in via interrupts.
	 */
	type = rb_head_page_set_update(cpu_buffer, next_page,
				       tail_page, RB_PAGE_HEAD);

	/*
	 * type can be one of four:
	 *  NORMAL - an interrupt already moved it for us
	 *  HEAD   - we are the first to get here.
	 *  UPDATE - we are the interrupt interrupting
	 *           a current move.
	 *  MOVED  - a reader on another CPU moved the next
	 *           pointer to its reader page. Give up
	 *           and try again.
	 */

	switch (type) {
	case RB_PAGE_HEAD:
		/*
		 * We changed the head to UPDATE, thus
		 * it is our responsibility to update
		 * the counters.
		 */
		local_add(entries, &cpu_buffer->overrun);
		local_sub(BUF_PAGE_SIZE, &cpu_buffer->entries_bytes);

		/*
		 * The entries will be zeroed out when we move the
		 * tail page.
		 */

		/* still more to do */
		break;

	case RB_PAGE_UPDATE:
		/*
		 * This is an interrupt that interrupt the
		 * previous update. Still more to do.
		 */
		break;
	case RB_PAGE_MOVED:
		/*
		 * The reader is on another CPU and just did
		 * a swap with our next_page.
		 * We never try again.
		 */
		return 1;
	/*
	 * An interrupt came in before the update and processed this for us.
	 * XXX: cannot happen since we are the host..
	 */
	case RB_PAGE_NORMAL:
	default:
		BPF_BUG_ON(1); /* WTF??? */
		return 1;
	}

	/*
	 * Now that we are here, the old head pointer is
	 * set to UPDATE. This will keep the reader from
	 * swapping the head page with the reader page.
	 * The reader (on another CPU) will spin till
	 * we are finished.
	 *
	 * We just need to protect against interrupts
	 * doing the job. We will set the next pointer
	 * to HEAD. After that, we set the old pointer
	 * to NORMAL, but only if it was HEAD before.
	 * otherwise we are an interrupt, and only
	 * want the outer most commit to reset it.
	 */
	new_head = next_page;

	rb_inc_page(cpu_buffer, &new_head);

	ret = rb_head_page_set_head(cpu_buffer, new_head, next_page,
				    RB_PAGE_NORMAL);

	/* code for handling interrupts was removed */

	/*
	 * If this was the outer most commit (the one that
	 * changed the original pointer from HEAD to UPDATE),
	 * then it is up to us to reset it to NORMAL.
	 */
	if (type == RB_PAGE_HEAD) {
		ret = rb_head_page_set_normal(cpu_buffer, next_page,
					      tail_page, RB_PAGE_UPDATE);
		BPF_BUG_ON(ret != RB_PAGE_UPDATE);
	}

	return 0;
}


/*
 * Changes:
 * 1. There are no interrupts, get rid of barriers, etc.
 */
static __always_inline
void rb_tail_page_update(struct ring_buffer_per_cpu *cpu_buffer,
			 struct buffer_page *tail_page,
			 struct buffer_page *next_page)
{
	unsigned long old_entries;
	unsigned long old_write;

	/*
	 * The tail page now needs to be moved forward.
	 *
	 * We need to reset the tail page, but without messing
	 * with possible erasing of data brought in by interrupts
	 * that have moved the tail page and are currently on it.
	 *
	 * We add a counter to the write field to denote this.
	 */
	old_write = local_add_return(RB_WRITE_INTCNT, &next_page->write);
	old_entries = local_add_return(RB_WRITE_INTCNT, &next_page->entries);

	/*
	 * Just make sure we have seen our old_write and synchronize
	 * with any interrupts that come in.
	 */
	barrier();

	/*
	 * If the tail page is still the same as what we think
	 * it is, then it is up to us to update the tail
	 * pointer.
	 */
	if (tail_page == READ_ONCE(cpu_buffer->tail_page)) {
		/* Zero the write counter */
		unsigned long val = old_write & ~RB_WRITE_MASK;
		unsigned long eval = old_entries & ~RB_WRITE_MASK;

		/*
		 * This will only succeed if an interrupt did
		 * not come in and change it. In which case, we
		 * do not want to modify it.
		 */
		(void)local_cmpxchg(&next_page->write, old_write, val);
		(void)local_cmpxchg(&next_page->entries, old_entries, eval);

		/*
		 * No need to worry about races with clearing out the commit.
		 * it only can increment when a commit takes place. But that
		 * only happens in the outer most nested commit.
		 */
		local_set(&next_page->page->commit, 0);

		/*
		 * Again, either we update tail_page or an interrupt does.
		 * But since there are no interrupts, just ignore
		 */
		(void)cmpxchg(&cpu_buffer->tail_page, tail_page, next_page);
	}
}

static __always_inline
void *__rb_page_index(struct buffer_page *bpage, unsigned index)
{
	return bpage->page->data + index;
}

static __always_inline
void rb_event_set_padding(struct ring_buffer_event *event)
{
	/* padding has a NULL time_delta */
	event->type_len = RINGBUF_TYPE_PADDING;
	event->time_delta = 0;
}

/*
 * We give v_tail_page from info as well, since otherwise the verifier
 * fails to figure out it was already verified and fails
 */
static __always_inline void
rb_reset_tail(struct ring_buffer_per_cpu *cpu_buffer, unsigned long tail,
	      struct rb_event_info /*__host*/ *info)
{
	struct buffer_page *tail_page = info->tail_page;
	struct ring_buffer_event *event;
	unsigned long length = info->length;

	/*
	 * Only the event that crossed the page boundary
	 * must fill the old tail_page with padding.
	 */
	if (tail >= BUF_PAGE_SIZE) {
		/*
		 * If the page was filled, then we still need
		 * to update the real_end. Reset it to zero
		 * and the reader will ignore it.
		 */
		if (tail == BUF_PAGE_SIZE)
			tail_page->real_end = 0;

		local_sub(length, &tail_page->write);
		return;
	}

	event = __rb_page_index(tail_page, tail);
	kmemcheck_annotate_bitfield(event, bitfield);

	/* account for padding bytes */
	local_add(BUF_PAGE_SIZE - tail, &cpu_buffer->entries_bytes);

	/*
	 * Save the original length to the meta data.
	 * This will be used by the reader to add lost event
	 * counter.
	 */
	tail_page->real_end = tail;

	/*
	 * If this event is bigger than the minimum size, then
	 * we need to be careful that we don't subtract the
	 * write counter enough to allow another writer to slip
	 * in on this page.
	 * We put in a discarded commit instead, to make sure
	 * that this space is not used again.
	 *
	 * If we are less than the minimum size, we don't need to
	 * worry about it.
	 */
	if (tail > (BUF_PAGE_SIZE - RB_EVNT_MIN_SIZE)) {
		/* No room for any events */

		/* Mark the rest of the page with padding */
		rb_event_set_padding(event);

		/* Set the write back to the previous setting */
		local_sub(length, &tail_page->write);
		return;
	}

	/* Put in a discarded event */
	event->array[0] = (BUF_PAGE_SIZE - tail) - RB_EVNT_HDR_SIZE;
	event->type_len = RINGBUF_TYPE_PADDING;
	/* time delta must be non zero */
	event->time_delta = 1;

	/* Set write to end of buffer */
	length = (tail + length) - BUF_PAGE_SIZE;
	local_sub(length, &tail_page->write);
}

static __always_inline unsigned long rb_page_write(struct buffer_page *bpage)
{
	return bpf_local_read(&bpage->write) & RB_WRITE_MASK;
}

static __always_inline unsigned rb_page_commit(struct buffer_page *bpage)
{
	return local_read(&bpage->page->commit);
}

static __always_inline unsigned
rb_commit_index(struct ring_buffer_per_cpu *cpu_buffer)
{
	return rb_page_commit(cpu_buffer->commit_page);
}

/*
 * Changes:
 * 1. Removing again and loop, anyhow we don't have interrupts
 * 2. Trying to get rid of READ_ONCE
 */
static __always_inline void
rb_set_commit_to_write(struct ring_buffer_per_cpu *cpu_buffer)
{
	/*
	 * We only race with interrupts and NMIs on this CPU.
	 * If we own the commit event, then we can commit
	 * all others that interrupted us, since the interruptions
	 * are in stack format (they finish before they come
	 * back to us). This allows us to do a simple loop to
	 * assign the commit to the tail.
	 */

	/* since we don't race with anyone we can avoid the READ_ONCE */
	/* XXX: THIS SHOULD ALWAYS BE TRUE */
	if (cpu_buffer->commit_page != cpu_buffer->tail_page) {
		//BPF_BUG_ON(rb_is_reader_page(v_cpu_buffer->tail_page));
		local_set(&cpu_buffer->commit_page->page->commit,
			  rb_page_write(cpu_buffer->commit_page));
		rb_inc_page(cpu_buffer, &cpu_buffer->commit_page);

		/* XXX: notice the v_commit_page may change */
		/* Only update the write stamp if the page has an event */
		if (rb_page_write(cpu_buffer->commit_page))
			cpu_buffer->write_stamp =
				cpu_buffer->commit_page->page->time_stamp;
		/* add barrier to keep gcc from optimizing too much */
	}
	if (rb_commit_index(cpu_buffer) !=
	       rb_page_write(cpu_buffer->commit_page)) {

		local_set(&cpu_buffer->commit_page->page->commit,
			  rb_page_write(cpu_buffer->commit_page));
//		RB_WARN_ON(cpu_buffer,
//			   local_read(&cpu_buffer->commit_page->page->commit) &
//			   ~RB_WRITE_MASK);
	}
}


/* Since we never retry, the output of this function is always NULL */
static struct ring_buffer_event *
rb_move_tail(struct ring_buffer_per_cpu *cpu_buffer,
	     unsigned long tail, struct rb_event_info /*__host*/ *info)
{
	struct buffer_page *tail_page = info->tail_page;
	struct buffer_page *commit_page = cpu_buffer->commit_page;
	struct ring_buffer *buffer = cpu_buffer->buffer;
	struct buffer_page *next_page;
	int ret;

	next_page = tail_page;

	rb_inc_page(cpu_buffer, &next_page);

	/*
	 * If for some reason, we had an interrupt storm that made
	 * it all the way around the buffer, bail, and warn
	 * about it.
	 */
	if (unlikely(next_page == commit_page)) {
		local_inc(&cpu_buffer->commit_overrun);
		goto out_reset;
	}

	/*
	 * This is where the fun begins!
	 *
	 * We are fighting against races between a reader that
	 * could be on another CPU trying to swap its reader
	 * page with the buffer head.
	 *
	 * We are also fighting against interrupts coming in and
	 * moving the head or tail on us as well.
	 *
	 * If the next page is the head page then we have filled
	 * the buffer, unless the commit page is still on the
	 * reader page.
	 */

	if (rb_is_head_page(cpu_buffer, next_page, &tail_page->list)) {
		/*
		 * If the commit is not on the reader page, then
		 * move the header page.
		 */
		if (!rb_is_reader_page(cpu_buffer->commit_page)) {
			/*
			 * If we are not in overwrite mode,
			 * this is easy, just stop here.
			 */

			if (!(buffer->flags & RB_FL_OVERWRITE)) {
				local_inc(&cpu_buffer->dropped_events);
				goto out_reset;
			}

			ret = bpf_rb_handle_head_page(cpu_buffer,
						  tail_page,
						  next_page);
			if (ret)
				goto out_reset;
		} else {
			/*
			 * We need to be careful here too. The
			 * commit page could still be on the reader
			 * page. We could have a small buffer, and
			 * have filled up the buffer with events
			 * from interrupts and such, and wrapped.
			 *
			 * Note, if the tail page is also the on the
			 * reader_page, we let it move out.
			 */
			if (unlikely((cpu_buffer->commit_page !=
				      cpu_buffer->tail_page) &&
				     (cpu_buffer->commit_page ==
				      cpu_buffer->reader_page))) {
				local_inc(&cpu_buffer->commit_overrun);
				goto out_reset;
			}
		}
	}

	rb_tail_page_update(cpu_buffer, tail_page, next_page);

out_reset:
	/* reset write */
	rb_reset_tail(cpu_buffer, tail, info);
	return NULL;
}

/*
 * The original functions return unsigned and not unsigned long, but this
 * causes additional unnecessary conversions.
 */
static __always_inline unsigned long
rb_event_index(struct ring_buffer_event *event)
{
	unsigned long addr = (unsigned long)event;

	return (addr & ~PAGE_MASK) - BUF_PAGE_HDR_SIZE;
}


/*
 * We do the comparison based on the host pointers since we cannot do
 * arithmetic operations with the host pointers.  Most likely the verifier is
 * afraid we are going to leak pointers.
 */
static __always_inline bool
rb_event_is_commit(struct ring_buffer_per_cpu *cpu_buffer,
		       struct ring_buffer_event *event)
{
	unsigned long addr = (unsigned long)event;
	unsigned long index;

	index = rb_event_index(event);
	addr &= PAGE_MASK;

	return cpu_buffer->commit_page->page == (void *)addr &&
		rb_commit_index(cpu_buffer) == index;
}

#define skip_time_extend(event) \
	((struct ring_buffer_event *)((char *)event + RB_LEN_TIME_EXTEND))

static __always_inline struct ring_buffer_event *
rb_add_time_stamp(struct ring_buffer_event *event, u64 delta)
{
	event->type_len = RINGBUF_TYPE_TIME_EXTEND;

	/* Not the first event on the page? */
	if (rb_event_index(event)) {
		event->time_delta = delta & TS_MASK;
		event->array[0] = delta >> TS_SHIFT;
	} else {
		/* nope, just zero it */
		event->time_delta = 0;
		event->array[0] = 0;
	}

	return skip_time_extend(event);
}


static __always_inline void
rb_update_event(struct ring_buffer_per_cpu *cpu_buffer,
		struct ring_buffer_event *event,
		struct rb_event_info /*__host*/ *info)
{
	unsigned length = info->length;
	u64 delta = info->delta;

	/* Only a commit updates the timestamp */
	if (unlikely(!rb_event_is_commit(cpu_buffer, event)))
		delta = 0;

	/*
	 * If we need to add a timestamp, then we
	 * add it to the start of the resevered space.
	 */
	if (unlikely(info->add_timestamp)) {
		event = rb_add_time_stamp(event, delta);
		length -= RB_LEN_TIME_EXTEND;
		delta = 0;
	}

	event->time_delta = delta;
	length -= RB_EVNT_HDR_SIZE;
	if (length > RB_MAX_SMALL_DATA || RB_FORCE_8BYTE_ALIGNMENT) {
		event->type_len = 0;
		event->array[0] = length;
	} else
		event->type_len = DIV_ROUND_UP(length, RB_ALIGNMENT);
}

static __always_inline struct ring_buffer_event *
__rb_reserve_next(struct ring_buffer_per_cpu *cpu_buffer,
		  struct rb_event_info /*__host*/ *info)
{
	struct ring_buffer_event *event;
	struct buffer_page *tail_page;
	unsigned long tail, write;

	/*
	 * If the time delta since the last event is too big to
	 * hold in the time field of the event, then we append a
	 * TIME EXTEND event ahead of the data event.
	 */
	if (unlikely(info->add_timestamp))
		info->length += RB_LEN_TIME_EXTEND;

	/* Don't let the compiler play games with cpu_buffer->tail_page */
	tail_page = info->tail_page = /*READ_ONCE*/(cpu_buffer->tail_page);
	write = local_add_return(info->length, &tail_page->write);

	/* set write to only the index of the write */
	write &= RB_WRITE_MASK;
	tail = write - info->length;

	/*
	 * If this is the first commit on the page, then it has the same
	 * timestamp as the page itself.
	 */
	if (!tail)
		info->delta = 0;

	/* See if we shot pass the end of this buffer page */
	if (unlikely(write > BUF_PAGE_SIZE))
		return rb_move_tail(cpu_buffer, tail, info);

	/* We reserved something on the buffer */

	event = __rb_page_index(tail_page, tail);
	kmemcheck_annotate_bitfield(event, bitfield);
	rb_update_event(cpu_buffer, event, info);

	local_inc(&tail_page->entries);

	/*
	 * If this is the first commit on the page, then update
	 * its timestamp.
	 */
	if (!tail)
		tail_page->page->time_stamp = info->ts;

	/* account for these added bytes */
	local_add(info->length, &cpu_buffer->entries_bytes);

	return event;
}

static __always_inline
void rb_end_commit(struct ring_buffer_per_cpu *cpu_buffer)
{
	unsigned long commits;

	BPF_BUG_ON(!local_read(&v_cpu_buffer->committing));

//again:
	commits = local_read(&cpu_buffer->commits);

	/* synchronize with interrupts */
	barrier();
	if (local_read(&cpu_buffer->committing) == 1)
		rb_set_commit_to_write(cpu_buffer);

	local_dec(&cpu_buffer->committing);

	/* synchronize with interrupts */
	barrier();

	/*
	 * Need to account for interrupts coming in between the
	 * updating of the commit page and the clearing of the
	 * committing counter.
	 */
	if (unlikely(local_read(&cpu_buffer->commits) != commits) &&
	    !local_read(&cpu_buffer->committing)) {
		local_inc(&cpu_buffer->committing);
		//goto again;
		// XXX: need to indicate an error
	}
}

static __always_inline void
rb_handle_timestamp(struct ring_buffer_per_cpu *cpu_buffer,
		    struct rb_event_info *info)
{
#if 0
	WARN_ONCE(info->delta > (1ULL << 59),
		  KERN_WARNING "Delta way too big! %llu ts=%llu write stamp = %llu\n%s",
		  (unsigned long long)info->delta,
		  (unsigned long long)info->ts,
		  (unsigned long long)cpu_buffer->write_stamp,
		  sched_clock_stable() ? "" :
		  "If you just came from a suspend/resume,\n"
		  "please switch to the trace global clock:\n"
		  "  echo global > /sys/kernel/debug/tracing/trace_clock\n");
#endif
	info->add_timestamp = 1;
}

/*
 * The following changes are done:
 * 1. Remove again, nr_loops, etc. because we cannot loop
 * 2. change test_time_stamp test to assignment to reduce branches
 * 3. Remvoing commit on behalf of others
 */

static __always_inline struct ring_buffer_event *
bpf_rb_reserve_next_event(struct ring_buffer *buffer,
			  struct ring_buffer_per_cpu *cpu_buffer,
			  unsigned long length)
{
	struct ring_buffer_event *event = NULL;
	struct rb_event_info info;
	u64 diff;

	rb_start_commit(cpu_buffer);
#ifdef CONFIG_RING_BUFFER_ALLOW_SWAP
	/*
	 * Due to the ability to swap a cpu buffer from a buffer
	 * it is possible it was swapped before we committed.
	 * (committing stops a swap). We check for it here and
	 * if it happened, we have to fail the write.
	 */
	barrier();
	if (unlikely(ACCESS_ONCE(cpu_buffer->buffer) != buffer)) {
		local_dec(&cpu_buffer->committing);
		local_dec(&cpu_buffer->commits);
		return NULL;
	}
#endif
	info.length = rb_calculate_event_length(length);

	info.add_timestamp = 0;
	info.delta = 0;

	info.ts = rb_time_stamp(cpu_buffer->buffer);
	diff = info.ts - cpu_buffer->write_stamp;

	/* make sure this diff is calculated here */
	barrier();

	/* Did the write stamp get updated already? */
	if (likely(info.ts >= cpu_buffer->write_stamp)) {
		info.delta = diff;

		/* This one should save a branch? */
		if (unlikely(test_time_stamp(info.delta)))
			rb_handle_timestamp(cpu_buffer, &info);
	}

	event = __rb_reserve_next(cpu_buffer, &info);

	if (unlikely(PTR_ERR(event) == -EAGAIN)) {
		if (info.add_timestamp)
			info.length -= RB_LEN_TIME_EXTEND;

		/* avoid the goto again course */
		goto out_fail;
	}

	if (!event)
		goto out_fail;

	return event;

out_fail:
	rb_end_commit(cpu_buffer);
	return NULL;
}

#define rb_reserve_next_event(a,b,c)	bpf_rb_reserve_next_event(a,b,c)

static struct ring_buffer_event *
bpf_ring_buffer_lock_reserve(struct ring_buffer *buffer,
			 unsigned long length)
{
	struct ring_buffer_per_cpu *cpu_buffer;
	struct ring_buffer_event *event = NULL;
	int cpu;

	if (unlikely(atomic_read(&buffer->record_disabled)))
		goto out;

	cpu = raw_smp_processor_id();

	if (unlikely(!cpumask_test_cpu(cpu, buffer->cpumask)))
		goto out;

	cpu_buffer = buffer->buffers[cpu];

	if (unlikely(atomic_read(&cpu_buffer->record_disabled)))
		goto out;

	if (unlikely(length > BUF_MAX_DATA_SIZE))
		goto out;

	/* We are never in recursive lock so no check is performed here */
	event = rb_reserve_next_event(buffer, cpu_buffer, length);
out:
	return event;
}

#define ring_buffer_lock_reserve(b,l)	bpf_ring_buffer_lock_reserve(b,l)

static __always_inline
int bpf_test_ti_thread_flag(struct thread_info *ti, int flag)
{
	return test_bit(flag, (unsigned long *)&ti->flags);
}

static __always_inline unsigned long bpf_current_top_of_stack(void)
{
	struct tss_struct *tss = this_cpu_ptr(KSYM(cpu_tss));

	/* only x86-64 supported */
	return tss->x86_tss.sp0;
}

#define current_top_of_stack()		bpf_current_top_of_stack()

static __always_inline struct thread_info *bpf_current_thread_info(void)
{
	return (struct thread_info *)(current_top_of_stack() - THREAD_SIZE);
}

#define current_thread_info()		bpf_current_thread_info()

#undef test_thread_flag
#define test_thread_flag(flag) \
	bpf_test_ti_thread_flag(current_thread_info(), flag)

#define tif_need_resched() test_thread_flag(TIF_NEED_RESCHED)


#define test_preempt_need_resched()	bpf_test_preempt_need_resched()

static __always_inline bool bpf_test_preempt_need_resched(void)
{
	return !(this_cpu_read(KSYM(__preempt_count)) & PREEMPT_NEED_RESCHED);
}


static __always_inline void
bpf_tracing_generic_entry_update(struct trace_entry *entry, unsigned long flags,
				 int pc)
{
	struct task_struct *tsk = bpf_get_current();

	entry->preempt_count		= pc & 0xff;
	entry->pid			= (tsk) ? tsk->pid : 0;
	entry->flags =
#ifdef CONFIG_TRACE_IRQFLAGS_SUPPORT
		(irqs_disabled_flags(flags) ? TRACE_FLAG_IRQS_OFF : 0) |
#else
		TRACE_FLAG_IRQS_NOSUPPORT |
#endif
		((pc & NMI_MASK    ) ? TRACE_FLAG_NMI     : 0) |
		((pc & HARDIRQ_MASK) ? TRACE_FLAG_HARDIRQ : 0) |
		((pc & SOFTIRQ_MASK) ? TRACE_FLAG_SOFTIRQ : 0) |
		(tif_need_resched() ? TRACE_FLAG_NEED_RESCHED : 0) |
		(test_preempt_need_resched() ? TRACE_FLAG_PREEMPT_RESCHED : 0);
}

#define tracing_generic_entry_update(e,f,p)				\
	bpf_tracing_generic_entry_update(e,f,p)

/* __always_inline for ring buffer fast paths */
static __always_inline void *
rb_event_data(struct ring_buffer_event *event)
{
	if (event->type_len == RINGBUF_TYPE_TIME_EXTEND)
		event = skip_time_extend(event);
	BUG_ON(event->type_len > RINGBUF_TYPE_DATA_TYPE_LEN_MAX);
	/* If length is in len field, then array[0] has the data */
	if (event->type_len)
		return (void *)&event->array[0];
	/* Otherwise length is in array[0] and array[1] has the data */
	return (void *)&event->array[1];
}

static __always_inline
void *bpf_ring_buffer_event_data(struct ring_buffer_event *event)
{
	return rb_event_data(event);
}

#define ring_buffer_event_data(a)	bpf_ring_buffer_event_data(a)

static __always_inline void
trace_event_setup(struct ring_buffer_event *event,
		  int type, unsigned long flags, int pc)
{
	struct trace_entry *ent = ring_buffer_event_data(event);

	tracing_generic_entry_update(ent, flags, pc);
	ent->type = type;
}

static __always_inline struct ring_buffer_event *
bpf_trace_buffer_lock_reserve(struct ring_buffer *buffer, int type,
			      unsigned long len, unsigned long flags,
			      int pc)
{
	struct ring_buffer_event *event;

	event = ring_buffer_lock_reserve(buffer, len);
	if (event != NULL)
		trace_event_setup(event, type, flags, pc);

	return event;
}

#define trace_buffer_lock_reserve(a,b,c,d,e) \
	bpf_trace_buffer_lock_reserve(a,b,c,d,e)

static __always_inline int bpf_preempt_count(void)
{
	return this_cpu_read(KSYM(__preempt_count)) & ~PREEMPT_NEED_RESCHED;
}

static __always_inline void
rb_update_write_stamp(struct ring_buffer_per_cpu *cpu_buffer,
		      struct ring_buffer_event *event)
{
	u64 delta;

	/*
	 * The event first in the commit queue updates the
	 * time stamp.
	 */
	if (rb_event_is_commit(cpu_buffer, event)) {
		/*
		 * A commit event that is first on a page
		 * updates the write timestamp with the page stamp
		 */
		if (!rb_event_index(event))
			cpu_buffer->write_stamp =
				cpu_buffer->commit_page->page->time_stamp;
		else if (event->type_len == RINGBUF_TYPE_TIME_EXTEND) {
			delta = event->array[0];
			delta <<= TS_SHIFT;
			delta += event->time_delta;
			cpu_buffer->write_stamp += delta;
		} else
			cpu_buffer->write_stamp += event->time_delta;
	}
}

static __always_inline void rb_commit(struct ring_buffer_per_cpu *cpu_buffer,
			     struct ring_buffer_event *event)
{
	local_inc(&cpu_buffer->entries);
	rb_update_write_stamp(cpu_buffer, event);
	rb_end_commit(cpu_buffer);
}
#if 0

static __always_inline int tick_nohz_tick_stopped(void)
{
	return this_cpu_read(KSYM(tick_cpu_sched)->tick_stopped);
}
#define tick_nohz_tick_stopped()	bpf_tick_nohz_tick_stopped()

/* TODO: fix the following */
#if 0
#undef test_cpu_cap
#define test_cpu_cap(c, bit)						\
	test_bit(bit, (unsigned long *)((c)->x86_capability))
#endif
#endif

/* TODO: fix to make boot_cpu_has work */
static __always_inline bool bpf_arch_irq_work_has_interrupt(void)
{
	return boot_cpu_has(X86_FEATURE_APIC);
}

#define arch_irq_work_has_interrupt() bpf_arch_irq_work_has_interrupt()

static __always_inline void arch_irq_work_raise(void)
{
	if (!arch_irq_work_has_interrupt())
		return;

	bpf_send_vcpu_self_ipi(get_ctx(), IRQ_WORK_VECTOR);
}

static bool bpf_llist_add_batch(struct llist_node *new_first,
				struct llist_node *new_last,
				struct llist_head *head)
{
	struct llist_node *first;

	new_last->next = first = ACCESS_ONCE(head->first);

	if (cmpxchg(&head->first, first, new_first) != first)
		BPF_BUG_ON(1);

	return !first;
}
#define llist_add_batch(nf, nl, h)	bpf_llist_add_batch(nf, nl, h)


static __always_inline bool bpf_llist_add(struct llist_node *p_new,
				 struct llist_head *p_head)
{
	return llist_add_batch(p_new, p_new, p_head);
}
#define llist_add(a,b)		bpf_llist_add(a,b)

/*
 * Need to be tri-state to indicate when there is a contention
 * XXX: we really need it to be lockless, but for the time being
 * we will live with interrupts being lost
 */
static __always_inline bool irq_work_claim(struct irq_work *work)
{
	unsigned long flags, oflags, nflags;

	/*
	 * Start with our best wish as a premise but only trust any
	 * flag value after cmpxchg() result.
	 */
	flags = work->flags & ~IRQ_WORK_PENDING;
	nflags = flags | IRQ_WORK_FLAGS;
	oflags = cmpxchg(&work->flags, flags, nflags);
	return !(oflags & IRQ_WORK_PENDING);
}

/* Enqueue the irq work @work on the current CPU */
static bool bpf_irq_work_queue(struct irq_work *work)
{
	/* Only queue if not already pending */
	/* XXX: how do we do that?? */
	if (!irq_work_claim(work))
		return false;

	/* XXX: no handling of the IRQ_WORK_LAZY */
	if (llist_add(&work->llnode, this_cpu_ptr(KSYM(raised_list))))
		arch_irq_work_raise();

	return true;
}
#define irq_work_queue(a)	bpf_irq_work_queue(a)

static __always_inline
void rb_wakeups(struct ring_buffer *buffer,
		struct ring_buffer_per_cpu *cpu_buffer)
{
	bool pagebusy;

	if (buffer->irq_work.waiters_pending) {
		buffer->irq_work.waiters_pending = false;
		/* irq_work_queue() supplies it's own memory barriers */
		irq_work_queue(&buffer->irq_work.work);
	}

	if (cpu_buffer->irq_work.waiters_pending) {
		cpu_buffer->irq_work.waiters_pending = false;
		/* irq_work_queue() supplies it's own memory barriers */
		irq_work_queue(&cpu_buffer->irq_work.work);
	}

	pagebusy = cpu_buffer->reader_page == cpu_buffer->commit_page;

	if (!pagebusy && cpu_buffer->irq_work.full_waiters_pending) {
		cpu_buffer->irq_work.wakeup_full = true;
		cpu_buffer->irq_work.full_waiters_pending = false;
		/* irq_work_queue() supplies it's own memory barriers */
		irq_work_queue(&cpu_buffer->irq_work.work);
	}
}

static __always_inline int bpf_ring_buffer_write(struct ring_buffer *buffer,
		      unsigned long length, void *data)
{
	struct ring_buffer_per_cpu *cpu_buffer;
	struct ring_buffer_event *event;
	void *body;
	int ret = -EBUSY;
	int cpu;

	preempt_disable_notrace();

	if (atomic_read(&buffer->record_disabled))
		goto out;

	cpu = raw_smp_processor_id();

	if (!cpumask_test_cpu(cpu, buffer->cpumask))
		goto out;

	cpu_buffer = buffer->buffers[cpu];

	if (atomic_read(&cpu_buffer->record_disabled))
		goto out;

	if (length > BUF_MAX_DATA_SIZE)
		goto out;

	if (unlikely(trace_recursive_lock(cpu_buffer)))
		goto out;

	event = rb_reserve_next_event(buffer, cpu_buffer, length);
	if (!event)
		goto out_unlock;

	body = rb_event_data(event);

	memcpy(body, data, length);

	rb_commit(cpu_buffer, event);

	rb_wakeups(buffer, cpu_buffer);

	ret = 0;

out_unlock:
	trace_recursive_unlock(cpu_buffer);

out:
	preempt_enable_notrace();

	return ret;
}
#define ring_buffer_write(a,b,c)	bpf_ring_buffer_write(a,b,c)

static __always_inline
int bpf_ring_buffer_unlock_commit(struct ring_buffer *buffer,
				  struct ring_buffer_event *event)
{
	struct ring_buffer_per_cpu *cpu_buffer;
	int cpu = raw_smp_processor_id();

	cpu_buffer = buffer->buffers[cpu];

	rb_commit(cpu_buffer, event);

	rb_wakeups(buffer, cpu_buffer);

	trace_recursive_unlock(cpu_buffer);

	preempt_enable_notrace();

	return 0;
}
#define ring_buffer_unlock_commit(b,e) bpf_ring_buffer_unlock_commit(b,e)

static __always_inline
void bpf__buffer_unlock_commit(struct ring_buffer *buffer,
			       struct ring_buffer_event *event)
{
	__this_cpu_write(*(bool*)KSYM(trace_cmdline_save), true);

	/* If this is the temp buffer, we need to commit fully */
	if (this_cpu_read(KSYM(trace_buffered_event)) == event) {
		/* Length is in event->array[0] */
		ring_buffer_write(buffer, event->array[0], &event->array[1]);
		/* Release the temp buffer */
		this_cpu_dec(KSYM(trace_buffered_event_cnt));
	} else
		ring_buffer_unlock_commit(buffer, event);
}

#define __buffer_unlock_commit(b,e)	bpf__buffer_unlock_commit(b,e)

static __always_inline void const_memcpy(char __host *a, const char *b, int len)
{
	int i;

#pragma unroll
	for (i = 0; i + sizeof(u64) <= len; i += sizeof(u64))
		*(u64 __host*)(a + i) = *(const u64*)(b + i);
#pragma unroll
	for (i = len - (len % sizeof(u64)); i + sizeof(u32) <= len; i += sizeof(u32))
		*(u32 __host*)(a + i) = *(const u32*)(b + i);
#pragma unroll
	for (i = len - (len % sizeof(u32)); i + sizeof(u16) <= len; i += sizeof(u16))
		*(u16 __host*)(a + i) = *(const u16*)(b + i);
#pragma unroll
	for (i = len - (len % sizeof(u16)); i + sizeof(u8) < len; i += sizeof(u8))
		*(u8 __host*)(a + i) = *(const u8*)(b + i);
}

static __always_inline int u64_hexstr_bytes(u64 value, int bytes)
{
	int i;

#pragma unroll
	for (i = bytes * 2 - 1; i > 0; i++) {
		if (value & (0xFFull << (i * 4)))
			break;
	}
	return i;
}

static __always_inline void u64_to_hexstr(char *str, u64 value, int bytes)
{
	int i;
	char __host *host_str;

	//bytes = u64_hexstr_bytes(value, bytes);
	host_str = host_ptr(str) + bytes * 2 - 1;
#pragma unroll
	for (i = 0; i < bytes * 2; i++, host_str--, value >>= 4) {
		u8 v = value & 0xf;

		if (i > 0 && value == 0)
			*host_str = ' ';
		else if (v < 10)
			*host_str = '0' + v;
		else
			*host_str = 'a' + v - 10;
	}
}

static __always_inline
void bpf_trace_array_vprintk(struct trace_array *tr, unsigned long ip,
			     u64 exit_reason)
{
	struct ring_buffer *buffer;
	struct ring_buffer_event *event;
	int size, pc;
	struct print_entry *entry;
	unsigned long flags;
	struct ring_buffer_per_cpu *cpu_buffer;
	int cpu = raw_smp_processor_id();
	const char *tbuf = "VM-exit: ip=0x";
	//const char *tbuf = "1234567890";
	//char __host *tbuffer = (const char __host *)(u64)tbuf;
	int len = strlen(tbuf) + 16;

	buffer = tr->trace_buffer.buffer;
	cpu_buffer = buffer->buffers[cpu];

	if (*vm_sym_ptr(tracing_disabled))
		return;

	pc = bpf_preempt_count();
	flags = bpf_get_vcpu_register(get_ctx(), HCB_REG_RFLAGS);
	size = sizeof(*entry) + len + 1;
	event = trace_buffer_lock_reserve(buffer, TRACE_PRINT, size,
					  flags, pc);

	if (event == NULL)
		return;

	entry = ring_buffer_event_data(event);
	entry->ip = ip;

	/*
	 * Since tbuffer is on the host, we need to call the underlying
	 * function.
	 */
	const char ** strs = *vm_sym_ptr(hcb_message_strings);
	const char * str =  (const char *)__bpf_alt_sym_map(strs);
	const_memcpy(host_ptr(entry->buf), tbuf, strlen(tbuf));
	u64_to_hexstr(entry->buf + strlen(tbuf), ip, 8);

	const_memcpy(host_ptr(entry->buf), tbuf, strlen(tbuf));
	entry->buf[len - 1] = '\0';
	entry->buf[len - 2] = '\n';
	__buffer_unlock_commit(buffer, event);
}

SEC("usercb/vm_ftrace")
int bpf_prog1(struct hcb_exit __host *h)
{
	volatile u64 ign = cache_ctx((void*)(u64)h);
	struct __hcb_area __host *a = &h->area;
	struct trace_array * tr = vm_sym_ptr(global_trace);

	/* TODO: mark that the buffer is of the host */

	bpf_trace_array_vprintk(tr, a->ip, h->exit_reason);
	return 0;
}



char _license[] SEC("license") = "GPL";
u32 _version SEC("version") = LINUX_VERSION_CODE;
