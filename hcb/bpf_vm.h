#ifndef _LINUX_BPF_VM_H
#define _LINUX_BPF_VM_H

#include "vmsym.h"
#include <asm/local.h>
#include <asm/page_64_types.h>
#include <uapi/linux/kvm.h>

#define __host		__attribute__((address_space(1)))
#define __guest		__attribute__((address_space(0)))

#define PROG(F) SEC("usercb/"__stringify(F)) int bpf_func_##F


#undef cmpxchg
#define cmpxchg(p, o, n)						\
	({								\
		__typeof__(*(p)) __host *h_p = host_ptr(p);		\
									\
		(__typeof__(*(p)))bpf_cmpxchg(h_p, sizeof(*h_p),	\
					      (u64)(o), (u64)(n));	\
	})


unsigned long long __read_ctx(unsigned long long, unsigned long long)
	asm("llvm.bpf.read.ctx");
//void cache_ctx(void) asm("llvm.bpf.cache.ctx");
u64 cache_ctx(void *ptr) asm("llvm.bpf.cache.ctx");
u64 __get_ctx(void) asm("llvm.bpf.get.ctx");

static __always_inline struct __hcb_area __host *get_ctx(void)
{
	return (struct __hcb_area __host*)__get_ctx();
}

static __always_inline u64 _read_ctx(u64 offset, int size)
{

	switch (size) {
	case 1:
		return __read_ctx(offset, BPF_B >> 3);
	case 2:
		return __read_ctx(offset, BPF_H >> 3);
	case 4:
		return __read_ctx(offset, BPF_W >> 3);
	case 8:
		return __read_ctx(offset, BPF_DW >> 3);
	default:
		__builtin_unreachable();
	}
}

static __always_inline __host void* __host_ptr(void *ptr)
{
	struct __hcb_area __host *a = get_ctx();
	u64 addr = (((u64)ptr - a->vm_base) & a->vm_mask) + a->vm_start;

	return (__host void*)addr;
}

#define host_ptr(p)							\
	({								\
		(__typeof__(*(p)) __host *)__host_ptr(p);		\
	})


extern const struct __hcb_area ___hcb_area;

#define read_ctx(f)							\
	({								\
		((__typeof__(___hcb_area.f))_read_ctx(			\
			offsetof(struct __hcb_area, f),			\
			sizeof(___hcb_area.f)));			\
	})

static __always_inline unsigned long get_alt_text_offset(void)
{
	return read_ctx(env[HCB_ENV_ALT_TEXT_OFFSET]);
}

static __always_inline unsigned long get_text_offset(void)
{
	return read_ctx(env[HCB_ENV_TEXT_OFFSET]);
}

static __always_inline unsigned long  __bpf_alt_sym_map(void __guest *ptr)
{
	return (unsigned long)ptr + get_alt_text_offset();
}

static __always_inline u32 bpf_smp_processor_id(void)
{
	return read_ctx(vcpu_id);
}

#define KSYM(s)			((__typeof__(s)*)(KSYM_ ## s))

/* TODO : implement BPF_BUG_ON debug */
#ifdef BPF_DEBUG
#define BPF_BUG_ON(cond)			do {} while (0)
#else
#define BPF_BUG_ON(cond)			do {} while (0)
#endif


#undef BUG_ON
#define BUG_ON(cond)		do {} while (0)


#define RUN_ONCE(x)		do {(x);} while (0)

#undef BUILD_BUG_ON
#define BUILD_BUG_ON(cond)	_Static_assert(!(cond), "BUILD_BUG_ON");

#undef WARN_ONCE
#define WARN_ONCE(condition, format...)	({	\
	int __ret_warn_once = !!(condition);	\
						\
	unlikely(__ret_warn_once);		\
})


/* XXX: do_nothing should evaluate the expression */


/* Overriding the kernel functions with BPF implementations */
#undef local_save_flags
#define local_save_flags(x)


#undef current
#define current bpf_get_current()

#undef preempt_count
#define preempt_count()				bpf_preempt_count()

#define local_inc(x)				bpf_local_inc(x)
#define local_dec(x)				bpf_local_dec(x)
#define local_add(x,y)				bpf_local_add(x,y)
#define local_sub(x,y)				bpf_local_sub(x,y)
#define local_add_return(x,y)			bpf_local_add_return(x,y)

#undef local_read
#define local_read(x)				bpf_local_read(x)

#undef local_set
#define local_set(x,y)				bpf_local_set(x,y)

#undef test_bit
#define test_bit(x,y)				bpf_test_bit(x,y)

#undef clear_bit
#define clear_bit(x,y)				__bpf_clear_bit(x,y)

extern unsigned long wrong_size_vm_read(void __guest *ptr)
	__noreturn;
extern unsigned long wrong_size_vm_write(void __guest *ptr)
	__noreturn;
extern unsigned long wrong_size_vm_read_atomic(volatile void __guest *ptr)
	__noreturn;
extern unsigned long wrong_size_vm_write_atomic(volatile void __guest *ptr)
	__noreturn;

/* 
 * local_inc uses x86 asm which is not supported by BPF
 *
 * Assuming that there is no higher priority event, we can get rid of the
 * volatile keyword 
 */
static __always_inline void bpf_local_set(local_t *l, long i)
{
	l->a.counter = i;
}

static __always_inline void bpf_local_add(long i, local_t *l)
{
	l->a.counter += i;
}

static __always_inline void bpf_local_sub(long i, local_t *l)
{
	bpf_local_add(-i, l);
}

static __always_inline void bpf_local_inc(local_t *l)
{
	bpf_local_add(1, l);
}

static __always_inline void bpf_local_dec(local_t *l)
{
	bpf_local_sub(1, l);
}

static __always_inline long bpf_local_add_return(long i, local_t *l)
{
	bpf_local_add(i, l);

	return l->a.counter;
}


/*
 * atomic.h
 */

static __always_inline long bpf_local_read(local_t *l)
{
	return l->a.counter;
}

static __always_inline int bpf_atomic_read(atomic_t *a)
{
	return *(volatile int*)&a->counter;
}

/*
 * atomic64.h
 */

static __always_inline u64 bpf_atomic64_read(atomic64_t *a)
{
	return *(volatile u64 *)&a->counter;
}

static __always_inline
u64 bpf_atomic64_cmpxchg(atomic64_t *v, long old, long new)
{
	return cmpxchg(&v->counter, old, new);
}


/*
 * bitops.h
 */

static __always_inline int bpf_test_bit(int nr,
			       const unsigned long *addr)
{
	const unsigned long *bit_addr;

	bit_addr = addr + nr / BITS_PER_LONG;

	return (*bit_addr) & (1ull << (nr % BITS_PER_LONG));
}

static __always_inline void __bpf_clear_bit(long nr, unsigned long *addr)
{
	unsigned char __host *bit_addr =
		host_ptr((unsigned char *)addr + nr / BITS_PER_BYTE);

	bpf_clear_bit((nr % BITS_PER_BYTE), bit_addr);
}

/*
 * cpumask.h
 */
static __always_inline int bpf_cpumask_test_cpu(int cpu,
						const struct cpumask *cpumask)
{
	return test_bit(cpumask_check(cpu), cpumask_bits((cpumask)));
}

static __always_inline void bpf_cpumask_clear_cpu(int cpu,
						  struct cpumask *cpumask)
{
	clear_bit(cpumask_check(cpu), cpumask_bits((cpumask)));
}

/*
 * string.h
 */

/*
 * We are going to force the caller to use host pointer
 */
static __always_inline
void __host* __bpf_memcpy(void __host *dst, const void __host *src, size_t size)
{
	/*
	 * Need to make the verifier sure about the size. As of 4.8,
	 * there are still no range checks in the verifier, so just
	 * limit to PAGE_MASK.
	 */
	size &= ~PAGE_MASK;

	bpf_memcpy(dst, size, src, size);

	return dst;
}

#undef memcpy
#define memcpy(d, s, l)		__bpf_memcpy(host_ptr(d), host_ptr(s), l)

/*
 * processor.h
 */
static __always_inline void bpf_load_cr3(pgd_t *pgdir)
{
	bpf_set_vcpu_register(get_ctx(), HCB_REG_CR3, (u64)pgdir, 0);
}
#define load_cr3(x)		bpf_load_cr3(x)



#define atomic_read(a)				bpf_atomic_read(a)
#define atomic64_read(a)			bpf_atomic64_read(a)
#define atomic64_cmpxchg(v, o, n)		bpf_atomic64_cmpxchg(v, o, n)

#define cpumask_test_cpu(x,y)			bpf_cpumask_test_cpu(x,y)
#define cpumask_clear_cpu(x,y)			bpf_cpumask_clear_cpu(x,y)

#define vm_sym_ptr(sym) ({							\
	((__typeof__(&sym))__bpf_alt_sym_map(KSYM(sym)));			\
})

extern unsigned long wrong_size_cmpxchg(volatile void *ptr)
	__noreturn;

static __always_inline unsigned long __cmpxchg_local_generic(volatile void *ptr,
		unsigned long old, unsigned long new, int size)
{
	unsigned long flags, prev;

	/*
	 * Sanity checking, compile-time.
	 */
	if (size == 8 && sizeof(unsigned long) != 8)
		wrong_size_cmpxchg(ptr);

	switch (size) {
	case 1: prev = *(u8 *)ptr;
		if (prev == old)
			*(u8 *)ptr = (u8)new;
		break;
	case 2: prev = *(u16 *)ptr;
		if (prev == old)
			*(u16 *)ptr = (u16)new;
		break;
	case 4: prev = *(u32 *)ptr;
		if (prev == old)
			*(u32 *)ptr = (u32)new;
		break;
	case 8: prev = *(u64 *)ptr;
		if (prev == old)
			*(u64 *)ptr = (u64)new;
		break;
	default:
		wrong_size_cmpxchg(ptr);
	}
	return prev;
}

#undef per_cpu_offset
static __always_inline unsigned long per_cpu_offset(int cpu)
{
	unsigned long *p = (unsigned long*)vm_sym_ptr(__per_cpu_offset) + cpu;

	return *p;
}

static __always_inline void *__per_cpu_ptr(void *ptr, int cpu)
{
	return (void*)(per_cpu_offset(cpu) + (unsigned long)ptr);
}

#undef smp_processor_id
#define smp_processor_id()	bpf_smp_processor_id()

#undef raw_smp_processor_id
#define raw_smp_processor_id()	bpf_smp_processor_id()


#undef per_cpu_ptr
#define per_cpu_ptr(ptr, cpu) ({						\
	((__typeof__((ptr)))__per_cpu_ptr(ptr, cpu));			\
})

#undef per_cpu_read
#define per_cpu_read(ptr, cpu) ({						\
	*(per_cpu_ptr(ptr, cpu));						\
})

#undef per_cpu_write
#define per_cpu_write(ptr, cpu, val) ({						\
	*(per_cpu_ptr(ptr, cpu)) = val;						\
})

#undef this_cpu_ptr
#define this_cpu_ptr(ptr) ({							\
	per_cpu_ptr(ptr, smp_processor_id());					\
})

#undef this_cpu_read
#define this_cpu_read(ptr) ({							\
	per_cpu_read(ptr, smp_processor_id());					\
})

#undef this_cpu_dec
#define this_cpu_dec(ptr) ({							\
	(*this_cpu_ptr(ptr))--;							\
})

#undef WARN
#define WARN(x, ...)

#undef this_cpu_write
#define this_cpu_write(ptr, val) ({						\
	per_cpu_write(&ptr, smp_processor_id(), val);				\
})

#undef __this_cpu_write
#define __this_cpu_write(p,v)		this_cpu_write(p,v)


#undef cmpxchg_local
#define cmpxchg_local(ptr, o, n) ({					       \
	((__typeof__(*(ptr)))__cmpxchg_local_generic((ptr), (unsigned long)(o),\
			(unsigned long)(n), sizeof(*(ptr))));		       \
})

#undef local_cmpxchg
#define local_cmpxchg(l, o, n) \
	(cmpxchg_local(&((l)->a.counter), (o), (n)))


#define DEFINE_VM_STRUCT_PTR(type)					\
									\
typedef struct bpf_##type##_ptr	{					\
	struct type *h;							\
	struct type __guest *g;						\
} bpf_##type##_ptr_t;

#define DEFINE_VM_PTR(type)						\
									\
typedef struct bpf_##type##_ptr	{					\
	type *h;							\
	type __guest *g;						\
} bpf_##type##_ptr_t;


/* 
 * Overriding some functions with empty implementation
 */

#define __do_nothing()			do {} while (0)

#undef kmemcheck_annotate_bitfield
#define kmemcheck_annotate_bitfield(ptr, name)	__do_nothing()

#undef preempt_disable_notrace
#define preempt_disable_notrace()		\
	do { } while (0)

#define trace_recursive_lock(a)		({ 0; })

#define trace_recursive_unlock(a)		\
	do { } while (0)

#undef unlikely
#define unlikely(x) (x)

/* XXX: find a way to do a compiler barrier; most likely it will be an overkill */
#undef barrier
#define barrier()	do {} while (0)

#undef virt_rmb
#define virt_rmb() barrier()

static __always_inline void save_error_line(int err_line)
{
	struct __hcb_ipi __host *h = (struct __hcb_ipi __host*)get_ctx();

	h->area.err_line = err_line;
}

#undef WARN
#define WARN()		save_error_line(__LINE__)

static __always_inline unsigned long get_psym(unsigned long ptr)
{
	return ptr + get_text_offset();
}

#define PKSYM(s)		((__typeof__(s)*)get_psym(KSYM_ ## s))



#endif

