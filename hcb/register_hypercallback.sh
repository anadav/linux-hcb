IN_F=./${2}_kern.o
OUT_F=./tmp1.bin
SECTION=usercb/vm_$2

# options for callbacks: traceprintk, ipi

echo $IN_F

objdump -h $IN_F |
  grep $SECTION |
    awk '{print "dd if='$IN_F' of='$OUT_F' bs=1 count=$[0x" $3 "] skip=$[0x" $6 "]"}' |
      bash

./register_hypercallback -f ./tmp1.bin -c $1

