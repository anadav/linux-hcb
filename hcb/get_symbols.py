#!/usr/bin/python
import re
import errno
import socket
import subprocess
import time
import sys
#import json
import errno
#import socket
import os
#import os.path
import shlex

import shutil
import ConfigParser
import binascii
import operator
from functools import partial
import time
import glob
import sys
import getopt
import subprocess
import time

def fls(v):
    r = 0
    while (v != 0):
        v >>= 1
        r += 1
    return r

def next_greater_power_of_2(x):
    return 2**(x-1).bit_length()

PAGE_SIZE = 0x1000

def main(argv=None):
    output_file = "vmsym.h"
#    input_file = "/proc/kallsyms"
    input_file = "../System.map"
    
    syms = {}

    if argv is None:
        argv = sys.argv
    try:
        try:
            opts, args = getopt.getopt(argv[1:], "hi:o:",
                ["help", "input", "output"])
        except getopt.error, msg:
            raise Usage(msg)
        for o in opts:
            if o[0] == '-i':
                input_file = o[1]
            if o[0] == '-o':
                output_file = o[1]
        # get path from command
    except Usage, err:
        print >>sys.stderr, err.msg
        print >>sys.stderr, "for help use --help"
        return 2

    try:
        fo = open(output_file, 'w');
    except:
        print "Error opening output file\n"
        sys.exit(1)

    fo.write("#ifndef _VMSYM_H\n")
    fo.write("#define _VMSYM_H\n\n")
   
    fo.write("\n/* symbols */\n")

    try:
        with open(input_file, 'r') as fi:
            for line in fi:
                fields = line.split(' ')
                # hacky way to avoid duplicate names in modules
                sym = fields[2].rstrip().split('\t')[0]
                val = fields[0].rstrip()
                if '.' in sym:
                    continue
                if sym in syms:
                    syms[sym]['dup'] = True
                    continue
                syms[sym] = {'val': val, 'dup' : False}

    except:
        print "Error opening input file\n"
        sys.exit(1)

    for sym in syms.keys():
        if (syms[sym]['dup']):
            continue
        fo.write("#define KSYM_{0}\t(0x{1}ull)\n".format(sym, syms[sym]['val']))

    fo.write("\n")
    fo.write("#endif\n")

if __name__ == "__main__":
    sys.exit(main())
 
