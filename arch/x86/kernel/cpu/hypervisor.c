/*
 * Common hypervisor code
 *
 * Copyright (C) 2008, VMware, Inc.
 * Author : Alok N Kataria <akataria@vmware.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE or
 * NON INFRINGEMENT.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include <linux/init.h>
#include <linux/export.h>
#include <linux/debugfs.h>
#include <asm/processor.h>
#include <asm/hypervisor.h>

extern unsigned long max_pfn;

static const __initconst struct hypervisor_x86 * const hypervisors[] =
{
#ifdef CONFIG_XEN
	&x86_hyper_xen,
#endif
	&x86_hyper_vmware,
	&x86_hyper_ms_hyperv,
#ifdef CONFIG_KVM_GUEST
	&x86_hyper_kvm,
#endif
};

const struct hypervisor_x86 *x86_hyper;
EXPORT_SYMBOL(x86_hyper);

static inline void __init
detect_hypervisor_vendor(void)
{
	const struct hypervisor_x86 *h, * const *p;
	uint32_t pri, max_pri = 0;

	for (p = hypervisors; p < hypervisors + ARRAY_SIZE(hypervisors); p++) {
		h = *p;
		pri = h->detect();
		if (pri != 0 && pri > max_pri) {
			max_pri = pri;
			x86_hyper = h;
		}
	}

	if (max_pri)
		pr_info("Hypervisor detected: %s\n", x86_hyper->name);
}

void init_hypervisor(struct cpuinfo_x86 *c)
{
	if (x86_hyper && x86_hyper->set_cpu_features)
		x86_hyper->set_cpu_features(c);
}

void __init init_hypervisor_platform(void)
{

	detect_hypervisor_vendor();

	if (!x86_hyper)
		return;

	init_hypervisor(&boot_cpu_data);

	if (x86_hyper->init_platform)
		x86_hyper->init_platform();
}

bool __init hypervisor_x2apic_available(void)
{
	return x86_hyper                   &&
	       x86_hyper->x2apic_available &&
	       x86_hyper->x2apic_available();
}

/*
 * Letting user space access physical base to optimize the BPF program.
 * This is just for a proof-of-concept. This is obviously very bad from the
 * perspective of security.
 */
static int phys_base_get(void *data, u64 *val)
{
	*val = phys_base;
	return 0;
}

DEFINE_SIMPLE_ATTRIBUTE(phys_base_fops,
		phys_base_get, NULL, "%llu\n");

static int __init phys_base_debugfs(void)
{
	void *ret;

	ret = debugfs_create_file("phys_base", 0444, NULL, NULL,
			&phys_base_fops);
	if (!ret)
		pr_warn("Failed to create phys_base in debugfs");
	return 0;
}
late_initcall(phys_base_debugfs);

/*
 * Letting userspace read page_offset (yes, it does defeat KASLR, but the
 * idea is to do it in a secure manner, and only allow administrators
 * to get this information.
 */
static int page_offset_get(void *data, u64 *val)
{
	*val = PAGE_OFFSET;
	return 0;
}

DEFINE_SIMPLE_ATTRIBUTE(page_offset_fops,
		page_offset_get, NULL, "%llu\n");

static int __init page_offset_debugfs(void)
{
	void *ret;

	ret = debugfs_create_file("page_offset", 0444, NULL, NULL,
			&page_offset_fops);
	if (!ret)
		pr_warn("Failed to create page_offset in debugfs");
	return 0;
}
late_initcall(page_offset_debugfs);

static int last_pfn_get(void *data, u64 *val)
{
	*val = max_pfn;
	return 0;
}

DEFINE_SIMPLE_ATTRIBUTE(last_pfn_fops,
		last_pfn_get, NULL, "%llu\n");

static int __init last_pfn_debugfs(void)
{
	void *ret;

	ret = debugfs_create_file("last_pfn", 0444, NULL, NULL,
			&last_pfn_fops);
	if (!ret)
		pr_warn("Failed to create last_pfn in debugfs");
	return 0;
}
late_initcall(last_pfn_debugfs);

static int phys_address_width_get(void *data, u64 *val)
{
	*val = fls64(max_pfn) + PAGE_SHIFT;
	return 0;
}

DEFINE_SIMPLE_ATTRIBUTE(phys_address_width_fops,
		phys_address_width_get, NULL, "%llu\n");

static int __init phys_address_width_debugfs(void)
{
	void *ret;

	ret = debugfs_create_file("phys_address_width", 0444, NULL, NULL,
			&phys_address_width_fops);
	if (!ret)
		pr_warn("Failed to create phys_address_width in debugfs");
	return 0;
}

late_initcall(phys_address_width_debugfs);
