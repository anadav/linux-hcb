#ifndef _UAPI__LINUX_KVM_PARA_H
#define _UAPI__LINUX_KVM_PARA_H

/*
 * This header file provides a method for making a hypercall to the host
 * Architectures should define:
 * - kvm_hypercall0, kvm_hypercall1...
 * - kvm_arch_para_features
 * - kvm_para_available
 */

/* Return values for hypercalls */
#define KVM_ENOSYS		1000
#define KVM_EFAULT		EFAULT
#define KVM_E2BIG		E2BIG
#define KVM_EPERM		EPERM

#define KVM_HC_VAPIC_POLL_IRQ		1
#define KVM_HC_MMU_OP			2
#define KVM_HC_FEATURES			3
#define KVM_HC_PPC_MAP_MAGIC_PAGE	4
#define KVM_HC_KICK_CPU			5
#define KVM_HC_MIPS_GET_CLOCK_FREQ	6
#define KVM_HC_MIPS_EXIT_VM		7
#define KVM_HC_MIPS_CONSOLE_OUTPUT	8
//#define KVM_HC_VM_KERNEL		9
#define KVM_HC_REGISTER_HCB		10

/* For queries (memory pinned) */
#define HCB_ENV_BASE			(0)

/* For notifications (no pinning) */
#define HCB_ENV_TEXT_OFFSET		(0)
#define HCB_ENV_ALT_TEXT_OFFSET		(1)

#define HCB_MAX_ENV_SIZE		(8)
#define HCB_MAX_MEMORY_REGIONS		(8)

struct hcb_memory_region {
	u64 start;
	u64 end;
};

struct hcb_registration {
	u64 prog_gva;
	u32 prog_len;
	u32 hcb_type;
	u32 mem_id;
	/* memory regions */
	u32 n_mr;
	struct hcb_memory_region mr[HCB_MAX_MEMORY_REGIONS];
	u32 ex_env;
	u64 env[0];
};

extern const char *hcb_message_strings[1];


/*
 * hypercalls use architecture specific
 */
#include <asm/kvm_para.h>

#endif /* _UAPI__LINUX_KVM_PARA_H */
